package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseURL = "/apitechu/v1";

    @GetMapping (APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }


    @GetMapping (APIBaseURL + "/products/{id}")
    public ProductModel getProductByID(@PathVariable String id){
        System.out.println("getProductByID()");
        System.out.println("El id es: "+ id);

        ProductModel resultado = new ProductModel();
        for (ProductModel product: ApitechuApplication.productModels ){
            if (product.getId().equals(id)){
                resultado = product;
            }
        }
        return resultado;
        //return new ProductModel();
    }

    @PostMapping (APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct()");
        System.out.println("El ID del nuevo producto es: "+newProduct.getId());
        System.out.println("La descripcion del nuevo producto es: "+newProduct.getDesc());
        System.out.println("El precio del nuevo producto es: "+newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;

    }

    @PutMapping (APIBaseURL + "/products/{id}")
    public ProductModel updateProduct (@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("updateProduct()");
        System.out.println("El id del la URL recibida es: " + id);
        System.out.println("El ID del producto a actualizar es: "+ product.getId());
        System.out.println("La descripcion del producto actualizado es: "+ product.getDesc());
        System.out.println("El precio del producto actualizado es: "+ product.getPrice());

        for (ProductModel productInList: ApitechuApplication.productModels ){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }
    @DeleteMapping (APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct (@PathVariable String id) {

        System.out.println("deleteProduct()");
        System.out.println("El id del la URL para borrar es: " + id);
       // System.out.println("El ID del producto actualizado es: " + product.getId());
       // System.out.println("La descripcion del producto actualizado es: " + product.getDesc());
       // System.out.println("El precio del producto actualizado es: " + product.getPrice());

        boolean foundProduct = false;
        ProductModel result = new ProductModel();
        for (ProductModel product: ApitechuApplication.productModels ){
           if (product.getId().equals(id)){
               System.out.println("El Producto encontrado para borrar es: " + product.getId());
               foundProduct = true;
               result = product;
            }
        }
        if (foundProduct == true){
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }
        return result;
    }

    //------------------------------------------------------------------------
    @PatchMapping (APIBaseURL + "/products/{id}")
    public ProductModel updatePatchProduct (@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updatePatchProduct()");
        System.out.println("El id del la URL recibida es: " + id);
        System.out.println("El ID del producto a actualizar es: "+ product.getId());

        String actualizadesc = product.getDesc();
        float actualizaprice = product.getPrice();
        System.out.println("Desc" + actualizadesc);
        System.out.println("Price" + actualizaprice);

        for (ProductModel productInList: ApitechuApplication.productModels ){
            if (productInList.getId().equals(id)){
                if (actualizadesc != null){
                    productInList.setDesc(actualizadesc);
                }
                if ((actualizaprice>0)){
                    productInList.setPrice(actualizaprice);
                }
            }
        }
        return product;
    }
}
