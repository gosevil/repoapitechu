package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;


@SpringBootApplication


public class ApitechuApplication {
//quito el static
	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);
		ApitechuApplication.productModels = ApitechuApplication.geTestData();

	}
//aqui quito el static
	private static ArrayList<ProductModel> geTestData(){

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(new ProductModel("1","Producto 1",10));
		productModels.add(new ProductModel("2","Producto 2",20.2f));
		productModels.add(new ProductModel("3","Producto 3",30.3f));
		productModels.add(new ProductModel("4","Producto 4",40));
		//productModels.remove(1);
		//System.out.println("El Product inicial 1 es:" + productModels.get(0) );
		//System.out.println("El Product inicial 2 es:" + productModels.get(1) );
		//System.out.println("El Product inicial 3 es:" + productModels.get(2) );
		//System.out.println("El Product inicial 4 es:" + productModels.get(3) );
		return productModels;
	}

}
